// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_WallDestroyer_generated_h
#error "WallDestroyer.generated.h already included, missing '#pragma once' in WallDestroyer.h"
#endif
#define SNAKEGAME_WallDestroyer_generated_h

#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWallDestroyer(); \
	friend struct Z_Construct_UClass_AWallDestroyer_Statics; \
public: \
	DECLARE_CLASS(AWallDestroyer, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AWallDestroyer)


#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAWallDestroyer(); \
	friend struct Z_Construct_UClass_AWallDestroyer_Statics; \
public: \
	DECLARE_CLASS(AWallDestroyer, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AWallDestroyer)


#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWallDestroyer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWallDestroyer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallDestroyer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallDestroyer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallDestroyer(AWallDestroyer&&); \
	NO_API AWallDestroyer(const AWallDestroyer&); \
public:


#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallDestroyer(AWallDestroyer&&); \
	NO_API AWallDestroyer(const AWallDestroyer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallDestroyer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallDestroyer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWallDestroyer)


#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_WallDestroyer_h_9_PROLOG
#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_INCLASS \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_WallDestroyer_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WallDestroyer_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AWallDestroyer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_WallDestroyer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
