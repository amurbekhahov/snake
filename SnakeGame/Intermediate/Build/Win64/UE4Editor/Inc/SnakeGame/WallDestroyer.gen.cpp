// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/WallDestroyer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWallDestroyer() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallDestroyer_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallDestroyer();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
// End Cross Module References
	void AWallDestroyer::StaticRegisterNativesAWallDestroyer()
	{
	}
	UClass* Z_Construct_UClass_AWallDestroyer_NoRegister()
	{
		return AWallDestroyer::StaticClass();
	}
	struct Z_Construct_UClass_AWallDestroyer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyRootComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWallDestroyer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallDestroyer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WallDestroyer.h" },
		{ "ModuleRelativePath", "WallDestroyer.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallDestroyer_Statics::NewProp_MyRootComponent_MetaData[] = {
		{ "Category", "WallDestroyer" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WallDestroyer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWallDestroyer_Statics::NewProp_MyRootComponent = { "MyRootComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWallDestroyer, MyRootComponent), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWallDestroyer_Statics::NewProp_MyRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWallDestroyer_Statics::NewProp_MyRootComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWallDestroyer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWallDestroyer_Statics::NewProp_MyRootComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWallDestroyer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWallDestroyer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWallDestroyer_Statics::ClassParams = {
		&AWallDestroyer::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWallDestroyer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWallDestroyer_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWallDestroyer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWallDestroyer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWallDestroyer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWallDestroyer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWallDestroyer, 3082768472);
	template<> SNAKEGAME_API UClass* StaticClass<AWallDestroyer>()
	{
		return AWallDestroyer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWallDestroyer(Z_Construct_UClass_AWallDestroyer, &AWallDestroyer::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AWallDestroyer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWallDestroyer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
